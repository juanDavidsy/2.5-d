using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using UnityEngine.UI;
using MoreMountains.InventoryEngine;
using System.Text.RegularExpressions;

public class FarmManager : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private LevelSelector levelSelector;
    [SerializeField] private Text cantidad,ValorTotal,time;
    [SerializeField] private GameObject loseImage,winImage,prefabWin,prefabPlayer;
    [SerializeField] private string nombreTradeInventario, nombreMainInventario;
    private Inventory inventoriaTrade, inventoriaMain;
    private int valorItem;
    private float tiempo;
    public bool gano;

    public static bool tutorial = true;

    public static bool plant = true;
    private int inventoryIndex;


    void Start()
    {
        gano = true;
        tiempo = int.Parse(time.text);
        CorgiEnginePointsEvent.Trigger(PointsMethods.Add, 100);
        prefabPlayer= GameObject.FindGameObjectWithTag("Player");
        inventoriaTrade = GameObject.FindGameObjectWithTag("Inventory").transform.Find(nombreTradeInventario).GetComponent<Inventory>();
        inventoriaMain = GameObject.FindGameObjectWithTag("Inventory").transform.Find(nombreMainInventario).GetComponent<Inventory>();
    }


    void Update()
    {
        if (tiempo>=0)
        {
            tiempo -= Time.deltaTime;
            time.text = tiempo.ToString();
        }else
        {
           
            if (gameManager.Points>=300 && gano==true)
            {
                gano = false;
                winImage.SetActive(true);
                prefabWin.SetActive(true);

                prefabPlayer.transform.position = prefabWin.transform.position - new Vector3(10, 0,0);
            }
            else if(gameManager.Points < 300)
            {
                loseImage.SetActive(true);
                StartCoroutine(WaitForLose());
            }
        }
    }

    IEnumerator WaitForLose()
    {
        yield return new WaitForSeconds(2);
        levelSelector.RestartLevel();
    }
    public void AddQuantity(bool sumar)
    {
        int num = sumar == true ? num = 1 : num = -1;
        int count = int.Parse(cantidad.text);
        count += num;
        count = Mathf.Clamp(count,0,10);
        cantidad.text = count.ToString();

        ValorTotal.text = (valorItem * count).ToString();
    }

    public void BuyItem()
    {
        if (ValorTotal.text!="0")
        {
            foreach (var item in inventoriaMain.Content)
            {
                if (item == null)
                {
                    CorgiEnginePointsEvent.Trigger(PointsMethods.Add, -int.Parse(ValorTotal.text));
                    inventoriaMain.AddItem(inventoriaTrade.Content[inventoryIndex],int.Parse(cantidad.text));
                    break;
                } 

            }

        }
    }

    public void SellItem()
    {
        if (ValorTotal.text != "0")
        {
            foreach (var item in inventoriaMain.Content)
            {
                if (item != null && item.name == inventoriaTrade.Content[inventoryIndex].ItemName)
                {
                    int count = 0;
                    if (item.Quantity < int.Parse(cantidad.text))
                        count = item.Quantity;
                    else
                        count = int.Parse(cantidad.text);

                    CorgiEnginePointsEvent.Trigger(PointsMethods.Add, valorItem * count);
                    inventoriaMain.RemoveItemByID(inventoriaTrade.Content[inventoryIndex].ItemID, count);
                    break;
                }
            }
        }
    }

    public void ItemSelection(InventorySlot slot)
    {
        
        inventoryIndex = slot.Index;
        ValorTotal.text = "0";
        cantidad.text = "0";
        valorItem = int.Parse(Regex.Replace(inventoriaTrade.Content[inventoryIndex].Description, "[^0-9]", ""));
    }

    public void clearWindow()
    {
        ValorTotal.text = "0";
        cantidad.text = "0";
        valorItem = 0;
    }
}
