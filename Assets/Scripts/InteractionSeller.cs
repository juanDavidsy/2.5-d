using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionSeller : MonoBehaviour
{
    [SerializeField] private GameObject dialogo, menuTrade, botonTrade, botones, menuDetalles;
    [SerializeField] private CanvasGroup mainMenu;
    [SerializeField] private FarmManager farmManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag=="Player")
        {
            
            if(FarmManager.tutorial==true)
            {
                dialogo.SetActive(true);
                FarmManager.tutorial=false;
            }
            botonTrade.SetActive(true);
            botones.SetActive(false);
            menuDetalles.SetActive(false);
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (mainMenu.alpha==0)
        {
            menuTrade.SetActive(false);
            farmManager.clearWindow();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            dialogo.SetActive(false);
            botonTrade.SetActive(false);
            botones.SetActive(true);
            menuTrade.SetActive(false);
            menuDetalles.SetActive(true);
        }
    }





}