using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InventoryEngine;
public class Plants_Controller : MonoBehaviour
{
    [SerializeField] private Transform corn;
    private Inventory inventoriaMain,inventoriaWeapon;
    [SerializeField] private float time;
    [SerializeField] private string nombreMainInventario, nombreWeaponInventario;
    [SerializeField] private InventoryItem nombreSemillas;
    [SerializeField] private GameObject prefabCorn,plantTutorial;


    void Start()
    {
        inventoriaMain = GameObject.FindGameObjectWithTag("Inventory").transform.Find(nombreMainInventario).GetComponent<Inventory>();
        inventoriaWeapon = GameObject.FindGameObjectWithTag("Inventory").transform.Find(nombreWeaponInventario).GetComponent<Inventory>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            if(FarmManager.plant == true)
            {
                plantTutorial.SetActive(true);
                FarmManager.plant = false;
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (corn.GetChild(corn.childCount-1).gameObject.activeInHierarchy==true && Input.GetButtonDown("Interaction"))
        {
            DesaparecerPlanta();
        }
        else if (collision.transform.tag == "Player" && Input.GetButtonDown("Interaction"))
        {
            foreach (var item in inventoriaMain.Content)
            {
                if (item!=null && inventoriaWeapon.Content[0] != null && inventoriaWeapon.Content[0].name == "InventoryHoe")
                {
                    
                    if (item.name == nombreSemillas.ItemName)
                    {
                        inventoriaMain.UseItem(nombreSemillas.ItemName);
                        StartCoroutine(WaitForGrowth());
                    }

                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            plantTutorial.SetActive(false);
        }
    }

    public IEnumerator WaitForGrowth()
    {
        transform.parent.GetComponent<MeshRenderer>().enabled = false;
        for (int i = 0; i < corn.childCount; i++)
        {
            corn.GetChild(i).gameObject.SetActive(true);
            yield return new WaitForSeconds(time);

        }

        yield return null;

    }

    public void DesaparecerPlanta()
    {
        for (int i = 0; i < corn.childCount; i++)
        {
            corn.GetChild(i).gameObject.SetActive(false);
        }
        GameObject go = Instantiate(prefabCorn, transform.position, Quaternion.identity);
    }

    


}
